# Built-in imports
from pprint import pprint

# Library imports
from arrow import Arrow

# Local imports
from anyhedge.contract import (
    ContractProposal,
    FeeAgreement,
    LongLeverage,
    Role,
    Sats,
    ScriptPriceInOracleUnitsPerBch,
    ScriptTimestamp,
    ShortLeverage,
    Side,
)
from anyhedge.oracle import (
    UsdEM2Beta,
)


start_timestamp = ScriptTimestamp(Arrow.utcnow().int_timestamp)
three_hours_in_seconds = 3 * 60 * 60
maturity_timestamp = ScriptTimestamp(start_timestamp + three_hours_in_seconds)

# Make a generic contract proposal
proposal = ContractProposal.new_from_intent(
    start_timestamp=ScriptTimestamp(start_timestamp),
    maturity_timestamp=ScriptTimestamp(maturity_timestamp),
    nominal_oracleUnits=UsdEM2Beta(1000_00.0),  # $1000
    long_leverage=LongLeverage(3.5),
    start_price_oracleUnits_per_bch=ScriptPriceInOracleUnitsPerBch(100_00),  # $100 / BCH
    maker_side=Side.SHORT,
    short_leverage=ShortLeverage(1),  # Definition of a strict hedge contract
)
pprint(proposal)
print(f'That is a ${proposal.short_input_oracleUnits.in_standard_units} {proposal.taker_side} Taker contract.')

# Make a specific contract funding
fee_agreements = [
    FeeAgreement(name='maker fee', amount_sats=Sats(1000), receiving=Role.MAKER, paying=Role.TAKER),
    FeeAgreement(name='settlement service fee', amount_sats=Sats(2000), receiving=Role.SETTLEMENT_SERVICE, paying=Role.TAKER),
]
funding = proposal.fund(fee_agreements=fee_agreements)
pprint(funding)
print(f'That is a contract funding with a total of {funding.fee_sats_to_maker.bch} BCH in fees to Maker.')


# Redeem the contract
redemption = funding.redeem(
    price_timestamp=maturity_timestamp,
    price_oracleUnits_per_bch=ScriptPriceInOracleUnitsPerBch(110_00),  # $110 / BCH
    force_maturity=False,
)
pprint(redemption)
print(f'That is a redemption paying {redemption.taker_payout_sats.bch} BCH to Taker '
      f'versus their original {redemption.base_funding.base_proposal.taker_input_sats.bch} BCH '
      f'for a gain of {redemption.taker_gain_percent_of_own_input}%.')
