from unittest import TestCase as _TestCaseForAssertions


_a = _TestCaseForAssertions()

instance = _a.assertIsInstance
equal = _a.assertEqual
almost_equal = _a.assertAlmostEqual
less_equal = _a.assertLessEqual
greater_equal = _a.assertGreaterEqual
